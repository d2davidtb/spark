<!DOCTYPE html>
<html>

<head>
    <meta http-eqapp/uiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SPARK</title>
    <link rel="icon" type="image/png" href="app/img/logos/rgroup-img.png"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <script charset="utf-8">
        $(function() {
            $("[data-toggle='tooltip']").tooltip();
        });
    </script>
</head>

<body>
    <div class="container-fluid"><?php include 'app/ui/header.php'; ?></div>
    <div class="container">
        <?php
        if (empty($_GET['pid'])) {
            include 'app/ui/home.php';
        } else {
            include 'app/ui/' . $_GET['pid'] . '.php';
        }
        include 'app/ui/footer.php';
        ?>
        <div class="text-center text-muted">
            &copy; Grupo de investigación SPARK <br>2024 - <?php echo date("Y") ?> All rights reserved
        </div>
    </div>
    <br>
</body>

</html>