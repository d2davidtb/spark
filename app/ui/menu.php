<nav class="navbar  navbar-expand-lg bg-body-tertiary">
	<a class="navbar-brand" href="index.php"><span class="fas fa-home" aria-hidden="true"></span></a>
	<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<!-- <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Dropdown </a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="index.php?pid=call">Equipo</a>
					<a class="dropdown-item" href="index.php?pid=dates">Sobre nosotros</a>
				</div>
			</li> -->
			<li class="nav-item">
				<a class="nav-link" href="index.php?pid=team">Equipo</a>
			</li>
			<div class="vr m-2"></div>
			<li class="nav-item"><a class="nav-link" href="index.php?pid=about">Sobre Nosotros</a></li>
		</ul>
	</div>
</nav>
