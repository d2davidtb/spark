<div class="container">
	<div class="row">
		<div class="col col-4">
			<img class="img-fluid" src="app/img/logos/rgroup-img.png" width="100%">
		</div>
		<div class="col col-8">
			<h2>Líneas de investigación</h2>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Mediciones de espectro</li>
				<li class="list-group-item">Antenas</li>
				<li class="list-group-item">Propagación</li>
				<li class="list-group-item">Ciencias de radio</li>
			</ul>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col col-9">
			<h2>Lineas de investigación de la Facultad Tecnológica</h2>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Innovación y desarrollo tecnológico local, regional y nacional.</li>
				<li class="list-group-item">Apoyo Tecnológico Empresarial.</li>
				<li class="list-group-item">Cultura, Tecnología y Educación.</li>
			</ul>
		</div>
		<div class="col col-3">
			<img class="img-fluid" src="app/img/logos/ud-tecno.png" width="100%">
		</div>
	</div>
</div>