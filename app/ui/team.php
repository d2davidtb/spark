<div class="container">
	<h1>Director</h1>
	<div class="row">
		<div class="card text-white bg-dark">
			<div class="card-header text-white"><strong>Director</strong></div>
			<div class="card-body">
				<div class="row">
					<div class="col-3"><img src="app/img/pictures/director.png" width="100%" class="img-thumbnail"></div>
					<div class="col-9">
						<strong>Marlon Patiño Bernal</strong>
						<hr>
						<div class="row align-items-center">
							<div class="col-2 text-center">
								<i class="fas fa-2x fa-envelope-open-text"></i>
							</div>
							<span class="col-10">
								marlonpb@udistrital.edu.co
							</span>
						</div>
						<div class="row align-items-center">
							<div class="col-2 text-center">
								<img src="app/img/logos/cvlac.png" width="50%"></img>
							</div>
							<div class="col-10">
								<a href="https://scienti.minciencias.gov.co/cvlac/visualizador/generarCurriculoCv.do?cod_rh=0000245186">
									CvLAC
								</a>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-2 text-center">
							<i class="fab fa-2x fa-orcid"></i>
							</div>
							<div class="col-10">
								<a href="https://orcid.org/0000-0003-2672-4616">
									ORCID
								</a>
							</div>
						</div>
						
						<br>
						<div class="row align-items-center">
							<div class="col-2 text-center">
								<i class="fas fa-2x fa-info-circle"></i>
							</div>
							<div class="col-10">
								<ul class="list-group list-group-flush">
									<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Profesor en la Universidad Distrital</li>
									<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Ph.D. en Ingeniería eléctrica</li>
									<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">M.Sc. en Telecomunicaciones</li>
									<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Especialista en Telecomunicaciones Móviles</li>
									<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Ingeniero Electrónico</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<hr>
		<h1>Con apoyo de: </h1>
		<div class="row">
			<div class="card text-white bg-dark">
				<div class="card-header text-white"><strong>Soporte</strong></div>
				<div class="card-body">
					<div class="row">
						<div class="col-3"><img src="app/img/pictures/developer.png" width="100%" class="img-thumbnail"></div>
						<div class="col-9">
							<strong>David Tabla</strong>
							<hr>
							<div class="row align-items-center">
								<div class="col-2 text-center">
									<i class="fas fa-2x fa-envelope-open-text"></i>
								</div>
								<div class="col-10">
									wdtablab@udistrital.edu.co
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-2 text-center">
									<i class="fab fa-2x fa-linkedin"></i>
								</div>
								<div class="col-10">
									<a href="https://www.linkedin.com/in/david-tabla-60b745167/">David Tabla</a>
								</div>
							</div>
							<br>
							<div class="row align-items-center">
								<div class="col-2 text-center">
									<i class="fas fa-2x fa-info-circle"></i>
								</div>
								<div class="col-10">
									<ul class="list-group list-group-flush">
										<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Estudiante de Ingeniería en Telemática de la Facultad Tecnológica de la Universidad Distrital</li>
										<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Devops Jr y desarrollador backend Semisenior</li>
										<li href="#" class="list-group-item list-group-item-action bg-transparent text-white">Tecnólogo en Sistematización de Datos</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>