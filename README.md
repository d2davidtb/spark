# SPARK Web page

## Requirements

Install PHP:
```
apt install php-common libapache2-mod-php php-cli
```

## Run server

Using VS Code, you can run server using component "PHP Server", following the next steps:

- Install component PHP Server
- Find `index.php` file
- Right click anywhere in the file
- Select `PHP Server: Serve project`
- Open `localhost:3000/` in your browser


# Contact

- David Tabla
  
  Email: d2davidtb@gmail.com
- Marlon Patiño Bernal
  
  Email: marlonpb@udistrital.edu.co
